import {
  Component, ViewChild, OnInit, OnDestroy, AfterViewInit, ElementRef, ChangeDetectorRef,
  OnChanges, SimpleChanges
} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Subscription } from 'rxjs/Subscription';
import { NgForm } from '@angular/forms';
import * as _ from 'lodash';
import { Store } from '@ngrx/store';
import {getParts, getPosterPagePosterSelector} from '../reducers';
import * as PartsAction from '../actions/parts.actions';
import {DragulaService} from 'ng2-dragula/ng2-dragula';

import {
  UserService,
  FirebaseService,
  ApplicationState
} from '../../common';
import { ApplicationState1 } from '../reducers';

import { PosterPageService } from '../../common/services/poster.page.service';

import { ThingType, PartType, PosterType, CommentType, PositionType, ISOType } from '../models';

@Component({
  selector: 'app-constructor',
  templateUrl: './constructor.component.html',
  styleUrls: ['./constructor.component.scss']
})
export class ConstructorContainerComponent implements OnInit, OnDestroy, AfterViewInit, OnChanges {
  @ViewChild('newPosterNameModal') private newPosterNameModal: ModalDirective;
  @ViewChild('waitingModal') private waitingModal: ModalDirective;
  @ViewChild('downloadModal') private downloadModal: ModalDirective;
  @ViewChild('paymentModal') private paymentModal: ModalDirective;
  @ViewChild('editCommentModal') private editCommentModal: ModalDirective;
  @ViewChild('readygModal') private readygModal: ModalDirective;
  @ViewChild('cardInfo') cardInfo: ElementRef;
  @ViewChild('contentPoster') contentPoster: ElementRef;
  @ViewChild('emailInfo') emailInfo: ElementRef;
  @ViewChild('nameOnCard') nameOnCard: ElementRef;
  card: any;
  cardHandler = this.onChange.bind(this);
  error: string;
  isview = true;
  poster: PosterType;
  createdPoster: PosterType = {};
  payOk = false;
  paymentId;

  newPosterNameForm: FormGroup;
  subscriptions: Subscription[] = [];

  public parts = [];
  public comments: CommentType[] = [];
  public commentText: string;
  private index: number;

  /**** PRICING ****/
  sponsEur = '5.90';
  tempEur = '19.90';
  fullEur = '39.90';
  sponsUsd = '6.90';
  tempUsd = '24.90';
  fullUsd = '49.90';
  eu = [this.sponsEur, this.tempEur, this.fullEur];
  us = [this.sponsUsd, this.tempUsd, this.fullUsd];
  current = this.eu;
  currentCurrency = '€';
  public iso: ISOType[] = [];
  orientation = 'PORTRAIT';
  isoUrls = [];
  isoHandler: any;

    constructor(
    public userService: UserService,
    public posterPageService: PosterPageService,
    private firebaseService: FirebaseService,
    private activatedRoute: ActivatedRoute,
    private store: Store<ApplicationState>,
    private store1: Store<ApplicationState1>,
    private cd: ChangeDetectorRef,
    private dragulaService: DragulaService,
    public element: ElementRef
    ) {
      // Dragula option config
      dragulaService.setOptions('bag-task1', {
        isContainer: function (el) {
            return el.classList.contains('drag-container'); // only elements in drake.containers will be taken into account
        },
        moves: function (el, source, handle, sibling) {
            return true; // elements are always draggable by default
        },
        accepts: function (el, target, source, sibling) {
            return true; // elements can be dropped in any of the `containers` by default
        },
        invalid: function (el, handle) {
            return false; // don't prevent any drags from initiating by default
        },
        direction: 'vertical',             // Y axis is considered when determining where an element would be dropped
        copy: false,                       // elements are moved by default, not copied
        copySortSource: false,             // elements in copy-source containers can be reordered
        revertOnSpill: false,              // spilling will put the element back where it was dragged from, if this is true
        removeOnSpill: false,              // spilling will `.remove` the element, if this is true
        mirrorContainer: document.body,    // set the element that gets mirror elements appended
        ignoreInputTextSelection: true     // allows users to select input text, see details below
      });
      dragulaService.drop.subscribe((value) => {
        this.isoHandler = value;
        const file = value.slice(1)['0'].childNodes[1].currentSrc.split('/').pop().split('?')[0];
        const fileName = file.substr(0, file.lastIndexOf('.'));

        const uuid = fileName.split('_')[2];
        const img = '/assets/gear-templates/iso/' + fileName + '.svg';
        const txt = this.isoHandler.slice(1)['0'].childNodes[3].value;
        const _top = this.isoHandler.slice(1)['0'].childNodes[1].x;
        const _left = this.isoHandler.slice(1)['0'].childNodes[1].y;
        this.isoHandler.slice(1)['0'].childNodes[3].id = uuid;

        const uniqueISO = this.iso.filter( function (item) {
          return item.uuid === uuid
        });

        if (uniqueISO.length === 0) {
          this.iso.push(
              {
                  uuid: uuid,
                  text: txt,
                  url: img,
                  position: {
                      left: _left,
                      top: _top
                  }
              });
        } else {
          uniqueISO.map(function(el) {
            el.text = txt;
            el.position.left = _left;
            el.position.top = _top;
          });
        }
        console.log(this.iso);

        this.onDrop(value.slice(1));
      });

    this.store1.select(getParts).subscribe(data => {
      this.parts = data['parts'];
    });
    this.store1.select(getPosterPagePosterSelector).subscribe(data => {
      if (data) {
        this.poster = data;
        if (this.poster.url) {
          this.createdPoster.jpg = this.poster.url + 'jpg';
          if (this.poster.stripeToken && this.poster.stripeToken.id) {
            this.createdPoster.pdf = this.poster.url + 'pdf';
          }
          this.payOk = false;
          this.paymentId = null;
          this.poster.stripeToken = null;
          this.poster.iso = this.iso;
        }
      }
    });
    for (let i = 1; i <= 26; i++) {
      if (i <= 9) {
        this.isoUrls.push('ISO_7010_M00' + i + '.png');
      } else {
        this.isoUrls.push('ISO_7010_M0' + i + '.png');
      }
    }
  }

    private hasClass(el: any, name: string) {
        return new RegExp('(?:^|\\s+)' + name + '(?:\\s+|$)').test(el.className);
    }

    private addClass(el: any, name: string) {
        if (!this.hasClass(el, name)) {
            el.className = el.className ? [el.className, name].join(' ') : name;
        }
    }
    private removeClass(el: any, name: string) {
        if (this.hasClass(el, name)) {
            el.className = el.className.replace(new RegExp('(?:^|\\s+)' + name + '(?:\\s+|$)', 'g'), '');
        }
    }
    private onDrag(args) {
        const [e, el] = args;
        console.log(e);
        console.log(el);
        this.removeClass(e, 'ex-moved');
    }
    private onDrop(args) {
        const [el, source, handle, sibling] = args;
    }

    private onOver(args) {
        const [e, el, container] = args;
        this.addClass(el, 'ex-over');
    }

    private onOut(args) {
        const [e, el, container] = args;
        this.removeClass(el, 'ex-over');
    }

    ngOnInit() {
    this.store.dispatch(new PartsAction.PartsLoadAction());
    this.posterPageService.getPoster(+this.activatedRoute.snapshot.params['id']);

    this.newPosterNameForm = new FormGroup({
      'name': new FormControl('', Validators.required)
    });

    this.subscriptions.push(
      this.posterPageService.openNewPosterNameModalSubject.subscribe(() => {
        this.openNewPosterNameModal();
      })
    );
    this.posterPageService.poster$.subscribe(data => {
      console.log('here!');
      console.log(data);
      if (data) {
        this.comments = data['comment'] || [];
      }
    });
  }

  showPayment(theProduct: string) {
    this.poster.product = theProduct;
    this.downloadModal.hide();
    this.paymentModal.show();
  }

  makePoster(theProduct: string) {
    this.poster.product = theProduct;
    this.downloadModal.hide();
    this.waitingModal.show();
    this.posterPageService.makePoster();
  }

  toggleCurrency(): void {
    console.log('CLICK!');
    if (this.currentCurrency === '€') {
      this.current = this.us;
      this.currentCurrency = '$';
    } else {
      this.current = this.eu;
      this.currentCurrency = '€';
    }
  }

  selectThing({ posterId, thing }: { posterId: number, thing: ThingType }) {
    if (posterId) {
      console.log('The posterId = ' + posterId);
    } else {
      console.log('There is no posterId');
    }
    console.log(thing);
    this.firebaseService.selectThing(posterId, thing);
  }

  deselectThing({ posterId, thing }: { posterId: number, thing: ThingType }) {
    this.firebaseService.deselectThing(posterId, thing);
  }

  manageComment({ posterId, part }: { posterId: number, part: PartType }) {
    console.log('The part = ' + part.uuid);
    this.firebaseService.managePart(posterId, part);
  }

  onChange({ error }) {
    if (error) {
      this.error = error.message;
    } else {
      this.error = null;
    }
    this.cd.detectChanges();
  }

  ngOnChanges(changes: SimpleChanges): void {
    const test = changes;
  }


  ngAfterViewInit() {

    this.card = elements.create('card', {
      hidePostalCode: true,
      style: {
        base: {
          iconColor: '#F99A52',
          color: '#202020',
          lineHeight: '48px',
          fontWeight: 400,
          fontFamily: '"Open Sans", "Helvetica Neue", "Helvetica", sans-serif',
          fontSize: '19px',

          '::placeholder': {
            color: 'gray',
          }
        },
      }
    });

    this.card.mount(this.cardInfo.nativeElement);
    this.card.addEventListener('change', this.cardHandler);
  }

  ngOnDestroy() {
    this.card.removeEventListener('change', this.cardHandler);
    this.card.destroy();
    this.posterPageService.cleanPosterPage();
    _.each(this.subscriptions, item => item.unsubscribe());
  }

  private openNewPosterNameModal() {
    this.newPosterNameModal.show();
  }

  async onSubmit(form: NgForm) {
    this.paymentModal.hide();
    this.waitingModal.show();
    console.log(this.card);
    this.poster.emailAddress = this.emailInfo.nativeElement.value;
    this.poster.nameOnCard = this.nameOnCard.nativeElement.value;
    const { token, error } = await stripe.createToken(this.card);

    if (error) {
      console.log('Something is wrong:', error);
    } else {
      console.log('Success!', token);
      this.payOk = true;
      // this.waitingModal.hide();
      // this.readygModal.show();
      this.card.clear();
      // ...send the token to the your backend to process the charge
      this.poster['stripeToken'] = token;
      this.paymentId = token.id.substr(4);
      this.posterPageService.makePoster();
    }
  }

  continue() {
    this.posterPageService.cleanPosterPageStripeData();
    this.readygModal.hide();
  }

  restart() {
    this.posterPageService.cleanPosterPage();
    this.readygModal.hide();
  }

  click(event) {
    const position: PositionType = {
      left: (event.pageX - this.contentPoster.nativeElement.offsetLeft) / this.contentPoster.nativeElement.offsetWidth * 100,
      top: (event.pageY - this.contentPoster.nativeElement.offsetTop - 10) / this.contentPoster.nativeElement.offsetHeight * 100
    }
    const cmt: CommentType = {
      content: '',
      editing: false,
      position
    }
    this.firebaseService.addComment(cmt);
  }

  editing(index) {
    this.index = index;
    this.commentText = this.comments[index].content;
    this.editCommentModal.show();
  }

  saveComment() {
    this.firebaseService.modifyComment(this.index, this.commentText);
    this.editCommentModal.hide();
  }

  deleteComment() {
    this.firebaseService.deleteComment(this.index);
    this.editCommentModal.hide();
  }

  toggleOrientation(): void {
    if (this.orientation === 'PORTRAIT') {
      this.orientation = 'LANDSCAPE';
    } else {
      this.orientation = 'PORTRAIT';
    }
      this.poster.orientation = this.orientation;
      console.log(this.orientation);
  }
  changeDescription(evt): void {
      const uuid = evt.target.id;
      this.iso.map(function(item) {
          if (item.uuid === uuid) {
            item.text = evt.target.value;
          }
      });
  };

}

import { Component, Input, Output, EventEmitter, ViewEncapsulation } from '@angular/core';
import { DragulaService } from 'ng2-dragula/ng2-dragula';

import { FirebaseService } from '../../../common/services/firebase.service';
import { PartType, PosterType } from '../../models';
import { ApplicationState1 } from '../../reducers';
import { Store } from '@ngrx/store';
import * as thingsAction from '../../actions/things.actions';

@Component({
  selector: 'gears-tabs',
  templateUrl: './gears-tabs.component.html',
  styleUrls: [ './gears-tabs.component.scss' ],
  encapsulation: ViewEncapsulation.None
})
export class GearsPartsComponent {
  /**
   * @Input
   */
  @Input() parts: PartType[];
  /**
   * @Input
   */
  @Input() poster: PosterType;
  /**
   * @Output
   */
  @Output() selectThing: EventEmitter<any> = new EventEmitter(false);
  /**
   * @Output
   */
  @Output() deselectThing: EventEmitter<any> = new EventEmitter(false);
  /**
   * @Output
   */
  @Output() manageComment: EventEmitter<any> = new EventEmitter(false);

  selectedIndex = 0;

  public things = {};

  constructor(
    private dragulaService: DragulaService,
    private firebaseService: FirebaseService,
    private store: Store<ApplicationState1>
  ) {
    dragulaService.dropModel.subscribe(() => {
      firebaseService.reorder(this.parts);
    });
    // this.store.select("things").subscribe(data=>{
    //   console.log(data);
    // });
    this.store.dispatch(new thingsAction.ThingsLoadAction());

    this.store.select('things').subscribe(things => {
      this.firebaseService.groupedThings$.subscribe(data => {
        console.log(data);
        this.things = data;
      })
    });
  }

  /**
   * @param index The index of selected tab
   * @returns false if selected tab is disabled
   */
  selectPart(index) {
    const selectedPart = this.parts[index];
    this.selectedIndex = index;
    console.log(this.selectedIndex);
  }
}

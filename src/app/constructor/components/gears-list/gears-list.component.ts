import {
  Component, EventEmitter, Input, Output, ViewEncapsulation, OnInit, OnDestroy, ViewChild,
  OnChanges
} from '@angular/core';
import * as _ from 'lodash';
import { Subscription } from 'rxjs/Subscription';
import { hasGearInPoster } from '../../../common/services/utilities.service';

import {FormControl, FormGroup} from '@angular/forms';
import {ModalDirective} from 'ngx-bootstrap';
import { GearComponentService } from '../../../common/services/gear.component.service';

import {
  ThingType,
  PosterType,
  PartType
} from '../../models';

@Component({
  selector: 'gears-list',
  templateUrl: './gears-list.component.html',
  styleUrls: [ './gears-list.component.scss' ],
  encapsulation: ViewEncapsulation.None
})
export class GearsListComponent implements OnInit, OnDestroy, OnChanges {
  /**
   * @Input
   */
  @Input() selectedPart: PartType;
  /**
   * @Input
   */
  @Input() poster: PosterType;
  /**
   * @Input
   */
  @Input () groupedGears: any;
  /**
   * @Output
   */
  @Output() selectThingFunction: EventEmitter<any> = new EventEmitter(false);
  /**
   * @Output
   */
  @Output() deselectThingFunction: EventEmitter<any> = new EventEmitter(false);
  /**
   * @Output
   */
  @Output() manageCommentFunction: EventEmitter<any> = new EventEmitter(false);


  partCommentForm: FormGroup;
  subscriptions: Subscription[] = [];
  partGears: ThingType[] = [];

  @ViewChild('partCommentModal') private partCommentModal: ModalDirective;

  constructor(
    private gearComponentService: GearComponentService
  ) {}

  /**
   * Toggle gear
   */

  onSelectGear(gear: ThingType) {
    if (!_.isEmpty(gear)) {
      if (hasGearInPoster(this.poster, gear) ) {
        this.deselectThingFunction.emit(gear);
      } else {
        this.selectThingFunction.emit(gear);
      }
    }
  }

  /**
   * Change gender
   */
  onSelectGender(gear: ThingType) {
    if (!_.isEmpty(gear)) {
      if (!(
        this.poster
        && this.poster[this.selectedPart.uuid]
        && this.poster[this.selectedPart.uuid].uuid === gear.uuid
      )) {
        this.selectThingFunction.emit(gear);
        console.log(gear.uuid);
      }
    }
  }

  /**
   * Change gear color
   */
  onChangeColour(gear, [firstColour, secondColour, thirdColour]) {
    const modifiedGear = _.assign({}, gear, { firstColour, secondColour, thirdColour });

    this.selectThingFunction.emit(modifiedGear);
  }

  isGearSelected(gear: ThingType) {
    return this.poster && this.poster[this.selectedPart.uuid] && this.poster[this.selectedPart.uuid].uuid === gear.uuid;
  }

  ngOnInit() {
    this.partCommentForm = new FormGroup({
      'remark': new FormControl('')
    });

    this.subscriptions.push(
      this.gearComponentService.openPartCommentModalSubject.subscribe(() => {
        this.openPartCommentModal();
      })
    );
  }

  ngOnChanges(changes) {
    // Need to replace the groupedGear items with poster stuff items
    if (this.poster) {
      this.poster.stuff.forEach((item) => {
        if (item.gender && item.part) {
          let test = this;
          const index = _.findIndex(this.groupedGears[item.part][item.gender], (currentItem: ThingType) => {
            return currentItem.uuid == item.uuid;
          });
          this.groupedGears[item.part][item.gender][index] = item;
        }
      });
    }
    this.partGears = this.groupedGears[this.selectedPart.uuid] ? this.groupedGears[this.selectedPart.uuid][this.poster.gender.gender] : [];
  }

  /**
   * Unsubscribe all
   */
  ngOnDestroy() {
    _.each(this.subscriptions, item => item.unsubscribe());
  }


  private openPartCommentModal() {
    this.partCommentModal.show();
  }

  closePartCommentModal() {
    const test = this.partCommentForm.value.remark;
    this.selectedPart.comment.content = test;

    this.manageCommentFunction.emit(this.selectedPart);
    this.partCommentModal.hide();
    // this.gearComponentService.closePartCommentModalSubject.next(this.partCommentForm.value.comment);
  }

}

import {
  Component,
  Input,
  Output,
  OnInit,
  EventEmitter,
  OnDestroy, AfterViewInit, ViewChild
} from '@angular/core';

import { Observable, Subscription } from 'rxjs';
import {FormControl} from '@angular/forms';
import * as _ from 'lodash';

import { ThingType } from '../../models';

@Component({
  selector: 'gear',
  templateUrl: './gear.component.html',
  styleUrls: [ './gear.component.scss' ]
})
export class GearComponent implements OnInit {
  /**
   * @Input
   */
  @Input() gear: ThingType;
  /**
   * @Input
   */
  @Input() selected: boolean;
  /**
   * click for adding to poster
   * @Output
   */
  @Output() select: EventEmitter<any> = new EventEmitter(null);
  /**
   * @Output
   */
  @Output() changeColour: EventEmitter<any> = new EventEmitter(null);

  mergedColoursObservable: Observable<any>;

  public firstColourControl: FormControl;
  public secondColourControl: FormControl;
  public thirdColourControl: FormControl;
  private componentStateColours: string[] = [];

  ngOnInit() {
    const observablesForMerging = [];

    if (this.gear.firstColour) {
      this.firstColourControl = new FormControl(this.gear.firstColour);

      observablesForMerging.push(this.firstColourControl.valueChanges);
    }

    if (this.gear.secondColour) {
      this.secondColourControl = new FormControl(this.gear.secondColour);

      observablesForMerging.push(this.secondColourControl.valueChanges);
    }

    if (this.gear.thirdColour) {
      this.thirdColourControl = new FormControl(this.gear.thirdColour);

      observablesForMerging.push(this.thirdColourControl.valueChanges);
    }

    if (observablesForMerging[0]) {
      this.mergedColoursObservable = Observable.merge(...observablesForMerging)
        .map(() => [
          this.firstColourControl.value,
          this.secondColourControl ? this.secondColourControl.value : undefined,
          this.thirdColourControl ? this.thirdColourControl.value : undefined
        ]);

      this.mergedColoursObservable
        .debounce(() => Observable.timer(300))
        .subscribe((colours) => {
          this.changeColour.emit(colours);
          this.componentStateColours = colours;
        });
    }
  }

  onSelect(gear) {
    this.select.emit(_.assign({}, gear, {
      firstColour: this.componentStateColours[0] || this.gear.firstColour,
      secondColour: this.componentStateColours[1] || this.gear.secondColour,
      thirdColour: this.componentStateColours[2] || this.gear.thirdColour
    }));
  }
}

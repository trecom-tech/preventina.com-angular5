import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Effect, Actions } from '@ngrx/effects';
import { Store, Action } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import * as _ from 'lodash';

import * as posterPageActions from '../actions/poster.page.actions';
import * as posterActions from '../actions/poster.actions';
import { PosterPageService } from '../../common/services/poster.page.service';
import { FirebaseService } from '../../common/services';
import { getUserSelector } from '../../common/reducers';
import { ApplicationState1 } from '../reducers';
import { UserType } from '../../common/models/user.type';
import { makeDefaultPoster } from '../../common/services/utilities.service';
import { ApplicationState } from '../../common/reducers';
import {BackentinaService} from "../../common/services/backentina.service";
import {StripeTokenType} from "../models/stripe-token.type";
import {FireBasePosterService} from "../../common/services/poster.service";

@Injectable()
export class PosterPageEffect {
  private user$: Observable<UserType> = this.store.select(getUserSelector);

  constructor(
    private actions$: Actions,
    private store1: Store<ApplicationState1>,
    private store: Store<ApplicationState>,
    private firebaseService: FirebaseService,
    private posterPageService: PosterPageService,
    private backentinaService: BackentinaService,
    private router: Router,
    private fireBasePosterService: FireBasePosterService
  ) {}

  @Effect() getPoster$: Observable<Action> = this.actions$
    .ofType(posterPageActions.PosterPageActionTypes.POSTER_PAGE_GET)
    .switchMap((payload) => {
      return Observable.combineLatest(
        this.firebaseService.posters$,
        this.firebaseService.postersAreLoaded$.filter(data => data)
      )
        .take(1)
        .map(([posters]) => {
          const posterInStore = _.find(posters, { id: payload });
          const poster = !!posterInStore ? posterInStore : makeDefaultPoster();

          return new posterPageActions.PosterPageSetAction(poster);
        });
    });

  @Effect() makePoster$: Observable<Action> = this.actions$
    .ofType(posterPageActions.PosterPageActionTypes.POSTER_PAGE_MAKE_POSTER)
    .switchMap(() => {
      return this.posterPageService.poster$
        .take(1)
        .switchMap((poster) => {
          console.log('SENDING POSTER TO FIREBASE');
          this.fireBasePosterService.doCreatePoster(poster);
          console.log('SENDING POSTER TO BACKENTINA');
          return this.backentinaService.makePoster(poster)
            .map((data) => new posterPageActions.PosterPageMakeSuccessAction(data))
            .catch(error => Observable.of(new posterPageActions.PosterPageMakeFailAction()));
          /*return this.user$
            .take(1)
            .switchMap((user: UserType) => {
              if (!!user) {
                if (!poster.id) {
                  this.posterPageService.openNewPosterNameModalSubject.next();

                  return this.posterPageService.closeNewPosterNameModalSubject
                    .take(1)
                    .map(name => new posterActions.PosterSaveAction(_.assign({}, poster, { name })));
                } else {
                  return Observable.of(new posterActions.PosterSaveAction(poster));
                }
              } else {
                this.router.navigate(['/login']);

                // go to authorisation
                return Observable.of({ type: 'NULL' });
              }
            });*/
        });
    });

  @Effect({ dispatch: false })
    createPosterSuccess$ = this.actions$
    .ofType(posterActions.PosterActionTypes.POSTER_CREATE_SUCCESS)
    .map((action: posterActions.PosterCreateSuccessAction) => action.payload)
    .do((payload) => this.router.navigate(['/constructor', payload.id]));
}

import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { Store, Action } from '@ngrx/store';
import { ApplicationState1 } from '../reducers';
import * as partActions from '../actions/parts.actions';
import { FirebaseService } from '../../common/services';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class PartEffect {
    constructor(
        private store: Store<ApplicationState1>,
        private actions$: Actions,
        private firebaseService: FirebaseService
    ) {}

    /*@Effect() loadParts$: Observable<Action> = this.actions$
        .ofType(partActions.PartsActionTypes.PARTS_LOAD)
        .switchMap(payload => {
                return this.firebaseService.get().map(parts => new partActions.PartsLoadSuccessAction(parts)).
                    catch(error => Observable.of(new partActions.PartsLoadFailAction()))
            }
        );*/

    @Effect() loadParts$: Observable<Action> = this.actions$
        .ofType(partActions.PartsActionTypes.PARTS_LOAD)
        .switchMap(payload => {
            return this.firebaseService.getParts().map(parts => new partActions.PartsLoadSuccessAction(parts))
        }
    );

}

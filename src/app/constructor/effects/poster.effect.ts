import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { Action, Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/take';
import * as _ from 'lodash';
import { ApplicationState1 } from '../reducers';

import * as posterActions from '../actions/poster.actions';
import * as thingsActions from '../actions/things.actions';
import { FirebaseService } from '../../common/services';
import { PosterPageService } from '../../common/services/poster.page.service';
import { PosterEndpoint } from '../../common/endpoints/poster.endpoint';
import { ThingType } from '../models/thing.type';
import * as utilities from '../../common/services/utilities.service';
import 'rxjs/add/observable/combineLatest';
import * as firebase from 'firebase';
import {BackentinaService} from '../../common/services/backentina.service';

@Injectable()
export class PosterEffect {
  constructor(
    private store: Store<ApplicationState1>,
    private actions$: Actions,
    private posterEndpoint: PosterEndpoint,
    private firebaseService: FirebaseService,
    private posterPageService: PosterPageService
  ) {}

  @Effect() loadPostersInitially$: Observable<Action> = this.actions$
    .take(1)
    .switchMap(() => {
      return this.firebaseService.loadPosters()
        .switchMap((posters) => {
            let datas = [];
            for(let key in posters) {
              let obj = posters[key];
              obj['key'] = key;
              datas.push(obj);
            }
            posters = datas;
            return Observable.combineLatest(
              this.firebaseService.things$,
              this.firebaseService.parts$
            )
              .map(([things, parts]) => {
                const gearTypes = _.chain(parts)
                  .filter({ format: 0 })
                  .map('gearType')
                  .value();

                _.each(posters, poster => {
                  _.each(gearTypes, (item: string) => {
                    if (poster[item]) {
                      const gear = _.find(things, { uuid: poster[item].uuid });

                      if (_.isEmpty(gear))
                        delete poster[item];

                      /*if (!_.isEmpty(gear)) {
                        poster[item].template = gear.template;
                      } else {
                        delete poster[item];
                      }*/
                    }
                  });
                });

                return new posterActions.PosterLoadAllSuccessAction(posters);
              });
          }
        );
    });

    @Effect()
      loadPoster$ = this.actions$
      .ofType(posterActions.PosterActionTypes.POSTER_LOAD)
      .switchMap((payload) => {
        return this.posterEndpoint.loadPoster(payload)
          .switchMap((poster) => {
              return Observable.combineLatest(
                this.firebaseService.things$,
                this.firebaseService.parts$
              )
                .map(([things, parts]) => {

                  const gearTypes = _.chain(parts)
                    .filter({ format: 0 })
                    .map('gearType')
                    .filter((item: string) => poster[item])
                    .value();

                  /*
                  _.each(gearTypes, (item: string) => {
                    poster[item].template = _.find(things, { uuid: poster[item].uuid }).template;
                  });
                  */

                  return new posterActions.PosterLoadSuccessAction(poster);
                });
            }
          );
        });

  @Effect() loadPosters$: Observable<Action> = this.actions$
    .ofType(posterActions.PosterActionTypes.POSTER_LOAD_ALL)
    .switchMap(payload => {
        return this.firebaseService.loadPosters().map(data => new posterActions.PosterLoadAllSuccessAction(data)).
                catch(error => Observable.of(new posterActions.PosterLoadAllFailAction()));
    });

  @Effect()
    savePoster$ = this.actions$
    .ofType(posterActions.PosterActionTypes.POSTER_SAVE)
    .map((action: posterActions.PosterSaveAction) => action.payload)
    .do((payload) => payload.id ? new posterActions.PosterUpdateAction(payload) : new posterActions.PosterCreateAction(payload));

  @Effect() updatePoster$: Observable<Action> = this.actions$
    .ofType(posterActions.PosterActionTypes.POSTER_UPDATE)
    .switchMap((payload) => {
      return this.posterEndpoint.savePoster(payload)
        .map(poster => new posterActions.PosterUpdateSuccessAction(poster))
        .catch(error => Observable.of(new posterActions.PosterUpdateFailAction()));
    });

  @Effect() createPoster$: Observable<Action> = this.actions$
    .ofType(posterActions.PosterActionTypes.POSTER_CREATE)
    .switchMap((payload) => {
      return this.firebaseService.createPoster(payload).map(data => new posterActions.PosterCreateSuccessAction(data)).
        catch(error => Observable.of(new posterActions.PosterCreateFailAction()));
    });


  @Effect()
    deletePoster$ = this.actions$
    .ofType(posterActions.PosterActionTypes.POSTER_DELETE)
    .map((action: posterActions.PosterDeleteAction) => action.payload)
    .switchMap((payload) => {
      return this.posterEndpoint.deletePoster(payload)
        .map(() => new posterActions.PosterDeleteSuccessAction(payload))
        .catch(error => Observable.of(new posterActions.PosterDeleteFailAction()));
    });

  @Effect() managePart$: Observable<Action> = this.actions$
    .ofType(posterActions.PosterActionTypes.POSTER_MANAGE_PART)
    .map((action: posterActions.PosterManagePartAction) => action.payload)
    .switchMap(({ posterId, part }) => {
      return Observable.combineLatest(
        this.firebaseService.posters$,
        this.posterPageService.poster$
      )
        .take(1)
        .map(([posters, posterFromPosterPage]) => {
          let poster;
          if (posterId) {
            poster = _.find(posters, { id: posterId });
          } else {
            poster = posterFromPosterPage;
          }
          utilities.addPartToPoster(poster, part);

          return new posterActions.PosterUpdateStoreAction(poster);
        });

    });

    @Effect() addComment$: Observable<Action> = this.actions$
      .ofType(posterActions.PosterActionTypes.POSTER_ADD_COMMENT)
      .map((action: posterActions.PosterAddCommentAction) => action.payload)
      .switchMap(({comment, posterId}) => {
        return Observable.combineLatest(
          this.firebaseService.posters$,
          this.posterPageService.poster$
        )
          .take(1)
          .map(([posters, posterFromPosterPage]) => {
            let poster;
            if (posterId) {
              poster = _.find(posters, { id: posterId });
            } else {
              poster = posterFromPosterPage;
            }
            utilities.addComment(poster, comment);                                

            return new posterActions.PosterUpdateStoreAction(poster);
          });

      }
    );
    @Effect() modifyComment$: Observable<Action> = this.actions$
      .ofType(posterActions.PosterActionTypes.POSTER_MODIFY_COMMENT)
      .map((action: posterActions.PosterModifyCommentActions) => action.payload)
      .switchMap(({index, text, posterId}) => {
        return Observable.combineLatest(
          this.firebaseService.posters$,
          this.posterPageService.poster$
        )
          .take(1)
          .map(([posters, posterFromPosterPage]) => {
            let poster;
            if (posterId) {
              poster = _.find(posters, { id: posterId });
            } else {
              poster = posterFromPosterPage;
            }
            utilities.modifyComment(poster, index, text);
            return new posterActions.PosterUpdateStoreAction(poster);
          });

      }
    );

    @Effect() deleteComment$: Observable<Action> = this.actions$
      .ofType(posterActions.PosterActionTypes.POSTER_DELETE_COMMENT)
      .map((action: posterActions.PosterDeleteCommentActions) => action.payload)
      .switchMap(({index, posterId}) => {
        return Observable.combineLatest(
          this.firebaseService.posters$,
          this.posterPageService.poster$
        )
          .take(1)
          .map(([posters, posterFromPosterPage]) => {
            let poster;
            if (posterId) {
              poster = _.find(posters, { id: posterId });
            } else {
              poster = posterFromPosterPage;
            }
            utilities.removeComment(poster, index);                                

            return new posterActions.PosterUpdateStoreAction(poster);
          });

      }
    );

  @Effect() changeThing$: Observable<Action> = this.actions$
    .ofType(posterActions.PosterActionTypes.POSTER_CHANGE_THING)
    .map((action: posterActions.PosterChangeThingsAction) => action.payload)
    .switchMap(({ posterId, thing }) => {
      return Observable.combineLatest(
        this.firebaseService.posters$,
        this.posterPageService.poster$
      )
        .take(1)
        .map(([posters, posterFromPosterPage]) => {
          let poster;

          if (posterId) {
            poster = _.find(posters, { id: posterId });
          } else {
            poster = posterFromPosterPage;
          }

          if (thing.part !== 'gender' && thing.part !== 'skin') {
            // Check if the gear is in the poster to decide on ADD/REMOVE
            // but do this only if it's not a simple colour change !!!!!!!!!!!!!!!!
            if (!utilities.hasGearInPoster(poster, thing)) {
              thing.action = 'ADD';
            } else if (!utilities.isEqual(utilities.getGearFromPoster(poster, thing.uuid), thing)) {
              thing.action = 'ADD';
            }
            // Step one - remove object from array with same uuid as the new object
            utilities.removeGearFromPoster(poster, thing);

            // Step two - add newobject to the array IF selected NOT if deselected
            if (thing.action && thing.action === 'ADD') {
              utilities.addGearToPoster(poster, thing);
            }

            delete thing.action;

            // thing.action = undefined;
          } else {
            poster[thing.part] = _.assign({}, thing); // modify body colour
          }

          if (thing.part === 'gender') {
            const allUsedThingsAlternatives = utilities.getAllUsedThingsAlternatives(poster);


            const newStuff = [];
            this.firebaseService.things$
              .subscribe(things => {
                _.each(allUsedThingsAlternatives, (uuid: string) => {
                  const relatedThing = _.find(things, (item: ThingType) => {
                    return item.uuid === uuid;
                  });

                  if (relatedThing) {
                    newStuff.push(_.assign({}, relatedThing, _.pick(utilities.getGearFromPoster(poster, relatedThing.associateId), ['firstColour', 'secondColour', 'thirdColour'])));
                  }
                  else {
                    console.log('THE RELATED THING IS NULLLLLLLLLLLLL');
                  }
                });
                const currentSkin = poster.skin;
                const alternate = currentSkin.associateId;
                const newSkin = _.find(things, (o: ThingType) => {
                  return o.uuid === alternate;
                });
                poster.skin = newSkin;
                poster.gender.gender = thing.gender;
                poster.gender.uuid = thing.gender + '_gender';
                poster.gender.url = poster.gender.url.replace(poster.gender.gender, thing.gender);
                poster.stuff = newStuff;
              });
              // @TODO implement poster->parts->things here
          }

          if (thing.part === 'skin') {
            // change hand colour
            // step 1 find hand in old colour
            // const uuid: string = poster.gender.gender + '_hand_0' + poster.skin.counter
            const oldHand: ThingType = _.find(poster.stuff, { part: 'bodypart' });
            if (oldHand) {
              // step 2 remove hand in current colour
              utilities.removeGearFromPoster(poster, oldHand);
              // step 3 find hand in new colour
              const oldCounter = oldHand.counter;
              const newHand: ThingType = {
                ...oldHand, counter: thing.counter,
                uuid: oldHand.uuid.replace('_0' + oldCounter, '_0' + thing.counter),
                url: oldHand.url.replace('_0' + oldCounter, '_0' + thing.counter),
                associateId: oldHand.associateId.replace('_0' + oldCounter, '_0' + thing.counter)
              };
              // step 4 add hand in new colour
              utilities.addGearToPoster(poster, newHand);
            }
            else {
              console.log("THE OLD HAND IS NULLLLLLLLLLLLLLLL");
            }

          }
          return new posterActions.PosterUpdateStoreAction(poster);
        });
    });

  /*@Effect()
    changeThingCommentPosition$ = this.actions$
    .ofType(posterActions.PosterActionTypes.POSTER_CHANGE_THING_COMMENT_POSITION)
    .map((action: posterActions.PosterChangeThingCommentPositionAction) => action.payload)
    .switchMap(({ thingType, position: [left, top] }) => {
      return this.posterPageService.poster$
        .take(1)
        .map((poster) => {
          const changedPoster = _.assign({}, poster);

          changedPoster[thingType] = _.assign({}, poster[thingType]);
          changedPoster[thingType].comment.position = { top, left };

          this.store.dispatch(new thingsActions.ThingUpdateAction(changedPoster[thingType]));
          return new posterActions.PosterUpdateStoreAction(changedPoster);
        });
    });*/


}

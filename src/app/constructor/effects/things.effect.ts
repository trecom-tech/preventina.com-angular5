import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { Store, Action } from '@ngrx/store';
import { ApplicationState1 } from '../reducers';
import * as thingActions from '../actions/things.actions';
import { FirebaseService } from '../../common/services';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch'

@Injectable()
export class ThingEffect {
    constructor(
        private store: Store<ApplicationState1>,
        private actions: Actions,
        private thingservice: FirebaseService
    ){}

    @Effect() loadThings$: Observable<Action> = this.actions
        .ofType(thingActions.ThingsActionTypes.THINGS_LOAD)
        .switchMap(payload => {
            return this.thingservice.getThings().map(things => new thingActions.ThingsLoadSuccessAction({ things }))
            .catch(error => Observable.of(new thingActions.ThingsLoadFailAction()))
        });
}
import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { Store, Action } from '@ngrx/store';
import { ApplicationState1 } from '../reducers';
import * as svgActions from '../actions/svgs.actions';
import { FirebaseService } from '../../common/services';
import { Observable } from 'rxjs/Observable';
import { SvgParamType } from '../models';

@Injectable()
export class SvgEffect {
    constructor(
        private store: Store<ApplicationState1>,
        private actions: Actions,
        private svgservice: FirebaseService
    ){}


    @Effect() loadSvgs$: Observable<Action> = this.actions
        .ofType(svgActions.SvgsActionTypes.SVGS_LOAD_ALL)
        .switchMap(payload=>{
            return this.svgservice.getSvgs().map(svgs => new svgActions.SvgsLoadAllSuccessAction(svgs)).
                catch(error => Observable.of(new svgActions.SvgsLoadAllFailAction()))
        });


    @Effect() loadSvg$: Observable<Action> = this.actions
        .ofType(svgActions.SvgsActionTypes.SVGS_LOAD)
        .switchMap(payload=>{
            return this.svgservice.loadSvg(payload).map(svgs => new svgActions.SvgsLoadSuccessAction(svgs)).
                catch(error => Observable.of(new svgActions.SvgsLoadFailAction()))
        });
        
}
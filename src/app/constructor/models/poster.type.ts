import { ThingType } from './thing.type';
import { PartType, CommentType } from './part.type';
import {StripeTokenType} from './stripe-token.type';
import {ISOType} from './iso.type';


export interface PosterType {
  id?: string;
  uuid?: string;
  tempId?: string;
  name?: string;
  gender?: ThingType;
  skin?: ThingType;
  parts?: PartType[];
  stuff?: ThingType[];
  comment?: CommentType[];
  url?: string;
  saved?: boolean;
  saving?: boolean;
  stripeToken?: StripeTokenType;
  jpg?: string;
  pdf?: string;
  emailAddress?: string;
  nameOnCard?: string;
  creationDate?: string;
  product?: string;
  orientation?: string;
  iso?: ISOType[];
}



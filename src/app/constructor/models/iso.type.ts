export interface Position {
    left: number;
    top: number;
}

export interface ISOType {
    uuid?: string;
    url?: string;
    text?: string;
    position: Position;
}

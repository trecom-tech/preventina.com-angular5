

export interface ThingType {
  uuid: string;
  associateId: string;
  name?: string;
  part: string;
  url?: string;
  gender?: string;
  counter?: number;
  thumbnailUrl?: string;
  color?: string;
  firstColour?: string;
  secondColour?: string;
  thirdColour?: string;
  template?: boolean;
  action?: string;
  layer?: number;
}

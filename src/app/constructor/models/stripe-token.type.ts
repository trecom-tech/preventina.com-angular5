
export interface StripeTokenType {
  client_ip?: string;
  created?: number;
  id: string;
  object?: string;
  type?: string;
}

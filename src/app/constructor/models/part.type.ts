export interface PositionType {
  top: number;
  left: number;
}
export interface CommentType {
  content: string;
  position: PositionType;
  editing?: boolean;
}
export interface PartType {
  name?: string;
  comment?: CommentType;
  canHaveComment?: boolean;
  uuid?: string;
  level?: number;
}

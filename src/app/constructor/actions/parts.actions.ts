import { Action } from '@ngrx/store';

import { type } from '../../common/services/utilities.service';
import { PartType } from '../models';

/**
 * For each action type in an action group, make a simple
 * enum object for all of this group's action types.
 *
 * The 'type' utility function coerces strings into string
 * literal types and runs a simple check to guarantee all
 * action types in the application are unique.
 */
export const PartsActionTypes = {
  PARTS_REORDER: type('PARTS_REORDER'),
  PARTS_LOAD: type('PARTS_LOAD'),
  PARTS_LOAD_SUCCESS: type('PARTS_LOAD_SUCCESS'),
  PARTS_LOAD_FAIL: type('PARTS_LOAD_FAIL'),
};

/**
 * Every action is comprised of at least a type and an optional
 * payload. Expressing actions as classes enables powerful
 * type checking in reducer functions.
 *
 * See Discriminated Unions: https://www.typescriptlang.org/docs/handbook/advanced-types.html#discriminated-unions
 */
export class PartsReorderAction implements Action {
  type = PartsActionTypes.PARTS_REORDER;

  constructor(public payload: PartType[]) { }
}

export class PartsLoadAction implements Action {
  type = PartsActionTypes.PARTS_LOAD;
}

export class PartsLoadSuccessAction implements Action {
  type = PartsActionTypes.PARTS_LOAD_SUCCESS;
  constructor(public payload: any) {}
}

export class PartsLoadFailAction implements Action {
  type = PartsActionTypes.PARTS_LOAD_FAIL;
  constructor() {}
}

/**
 * Export a type alias of all actions in this action group
 * so that reducers can easily compose action types
 */
export type PartsActions = [
  PartsReorderAction,
  PartsLoadAction,
  PartsLoadSuccessAction,
  PartsLoadFailAction
];

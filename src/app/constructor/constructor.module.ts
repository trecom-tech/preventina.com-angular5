import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { ModalModule } from 'ngx-bootstrap/modal';
import { RouterModule } from '@angular/router';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { ConstructorContainerComponent } from './containers/constructor.component';
import { SharedModule } from '../common/share.module';
import { AuthorisedGuard } from '../common';
import { PosterEffect, PosterPageEffect, PartEffect, ThingEffect } from './effects';
import { reducers } from './reducers';
import { PosterPageService } from '../common/services/poster.page.service';
import { StaticGearComponent } from './components/static-gear';
import { GearComponentService } from '../common/services/gear.component.service';
// import { ThingsColouringService } from './services/things-colouring.service';
import { DragulaModule, DragulaService} from 'ng2-dragula/ng2-dragula';

import {
    PosterPreviewComponent,
    GearComponent,
    GearsPartsComponent,
    GearsListComponent
  } from './components';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    AccordionModule,
    ModalModule,
    ReactiveFormsModule,
    RouterModule.forChild([
      {
          path: ':id',
          component: ConstructorContainerComponent,
          canActivate: [AuthorisedGuard]
      }, {
          path: '',
          component: ConstructorContainerComponent,
      }
    ]),
    SharedModule,
    StoreModule.forFeature('constructor', reducers),
    EffectsModule.forFeature([
      PosterEffect,
      PosterPageEffect,
      PartEffect,
      ThingEffect
    ]),
      DragulaModule
  ],
  declarations: [
    ConstructorContainerComponent,
    PosterPreviewComponent,
    GearComponent,
    GearsPartsComponent,
    GearsListComponent,
    StaticGearComponent
  ],
  providers: [
    PosterPageService,
    GearComponentService
    // ThingsColouringService
  ],
})

export class ConstructorModule {}

import { Action } from '@ngrx/store';
// import '@ngrx/core/add/operator/select';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import * as _ from 'lodash';

import * as posterPageActions from '../actions/poster.page.actions';
import * as posterActions from '../actions/poster.actions';
import { PosterType } from '../models/poster.type';
import { makeDefaultPoster } from '../../common/services/utilities.service';

export interface State {
  poster: PosterType;
}

const initialState: State = {
  poster: null
};

export interface ActionWithPayload<T> extends Action {
  payload: T;
};

/**
 * POSTER_PAGE_SET & POSTER_UPDATE & POSTER_UPDATE_STORE & POSTER_CREATE_SUCCESS: Assign payload to poster
 * POSTER_PAGE_CLEAN: Set poster to null
 */
export function PosterPageReducer(state = initialState, action: ActionWithPayload<State>): State {
  switch (action.type) {
    case posterPageActions.PosterPageActionTypes.POSTER_PAGE_SET:
    case posterActions.PosterActionTypes.POSTER_UPDATE:
    case posterActions.PosterActionTypes.POSTER_UPDATE_STORE:
    case posterActions.PosterActionTypes.POSTER_CREATE_SUCCESS: {
      const _state = state;
      const _action = action;
      const _new_poster = _.assign({}, state.poster, action.payload);
      const _new_state = _.assign({}, state, {
        poster: _new_poster
      });
      return _new_state;
    }

    case posterPageActions.PosterPageActionTypes.POSTER_PAGE_CLEAN: {
      const new_poster = makeDefaultPoster();
      return _.assign({}, state, {
        poster: new_poster
      });
    }

    case posterPageActions.PosterPageActionTypes.POSTER_PAGE_CLEAN_STRIPE: {
      const new_poster: PosterType = makeDefaultPoster();
      new_poster.stuff = state.poster.stuff;
      new_poster.skin = _.assign({}, state.poster.skin);
      new_poster.gender = _.assign({}, state.poster.gender);
      new_poster.parts = state.poster.parts;
      new_poster.iso = state.poster.iso;
      console.log('---sdf')
      return _.assign({}, state, {
        poster: new_poster
      });
    }

    case posterPageActions.PosterPageActionTypes.POSTER_MAKE_SUCCESS: {
      const _state = state;
      const _new_poster = _.assign({}, _state.poster, {'url': action.payload['file']}, {'saved': true}, {'saving': false});
      const _new_state = _.assign({}, _state, {
        'poster': _new_poster
      });
      return _new_state;
    }

    case posterPageActions.PosterPageActionTypes.POSTER_MAKE_FAIL: {
      const _state = state;
      const _new_poster = _.assign({}, _state.poster, {'saved': false, 'saving': false});
      const _new_state = _.assign({}, _state, {
        'poster': _new_poster
      });
      return _new_state;
    }

    case posterPageActions.PosterPageActionTypes.POSTER_PAGE_MAKE_POSTER: {
      const _state = state;
      const _new_poster = _.assign({}, _state.poster, {'saved': false, 'saving': true});
      const _new_state = _.assign({}, _state, {
        'poster': _new_poster
      });
      return _new_state;
    }

    default: {
      return state;
    }
  }
}

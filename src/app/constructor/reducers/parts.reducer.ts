import { Action } from '@ngrx/store';
// import '@ngrx/core/add/operator/select';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import * as _ from 'lodash';

import * as partsActions from '../actions/parts.actions';
import { PartType } from '../models';

export interface State {
  parts: PartType[];
}

const initialState: State = {
  parts: []
};

export interface ActionWithPayload<T> extends Action {
  payload: T;
};

/**
 * PARTS_REORDER: Reorder parts
 */
export function PartsReducer(state = initialState, action: ActionWithPayload<PartType[]>): State {
  switch (action.type) {
    case partsActions.PartsActionTypes.PARTS_REORDER: {
      return _.assign({}, state, {
          parts: _.map(action.payload, (item: PartType, index) => {
            item.level = index;

            return _.assign({}, item);
          })
        }
      );
    }
    case partsActions.PartsActionTypes.PARTS_LOAD_SUCCESS: {
      return {parts: action.payload};
    }

    case partsActions.PartsActionTypes.PARTS_LOAD_FAIL: {
      return state;
    }

    default: {
      return state;
    }
  }
}

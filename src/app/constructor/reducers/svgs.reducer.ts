import { Action } from '@ngrx/store';
import '@ngrx/core/add/operator/select';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import * as _ from 'lodash';

import * as svgsActions from '../actions/svgs.actions';
import { SvgType } from '../models/svg.type';

export interface State {
  svgs: SvgType[];
}

const initialState: State = {
  svgs: []
};

export interface ActionWithPayload<T> extends Action {
  payload: T;
};

/**
 * SVGS_REORDER: Reorder svgs
 */
export function SvgsReducer(state = initialState, action: ActionWithPayload<State>): State {
  switch (action.type) {
    case svgsActions.SvgsActionTypes.SVGS_LOAD_SUCCESS: {
      return { svgs: action.payload.svgs };
    }

    case svgsActions.SvgsActionTypes.SVGS_LOAD_FAIL: {
      return state;
    }
    case svgsActions.SvgsActionTypes.SVGS_LOAD_ALL_SUCCESS: {
      return { svgs: action.payload.svgs };
    }

    case svgsActions.SvgsActionTypes.SVGS_LOAD_ALL_FAIL: {
      return state;
    }

    default: {
      return state;
    }
  }
}

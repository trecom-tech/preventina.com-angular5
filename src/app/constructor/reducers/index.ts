// import '@ngrx/core/add/operator/select';
import 'rxjs/add/operator/switchMap';
import { Observable } from 'rxjs/Observable';

/**
 * The compose function is one of our most handy tools. In basic terms, you give
 * it any number of functions and it returns a function. This new function
 * takes a value and chains it through every composed function, returning
 * the output.
 *
 * More: https://drboolean.gitbooks.io/mostly-adequate-guide/content/ch5.html
 */
import { compose, ActionReducerMap, createSelector, createFeatureSelector } from '@ngrx/store';

/**
 * Advanced logging for @ngrx/store applications, ported from redux-logger
 */
import { storeLogger } from 'ngrx-store-logger';

/**
 * combineReducers is another useful metareducer that takes a map of reducer
 * functions and creates a new reducer that stores the gathers the values
 * of each reducer and stores them using the reducer's key. Think of it
 * almost like a database, where every reducer is a table in the db.
 *
 * More: https://egghead.io/lessons/javascript-redux-implementing-combinereducers-from-scratch
 */
import { combineReducers } from '@ngrx/store';
import { environment } from '../../../environments/environment';

/**
 * Every reducer module's default export is the reducer function itself. In
 * addition, each module should export a type or interface that describes
 * the state of the reducer plus any selector functions. The `* as`
 * notation packages up all of the exports into a single object.
 */
// data
// import * as fromAuthReducer from './reducers/auth.reducer';
import * as fromPartsReducer from './parts.reducer';
import * as fromPosterReducer from './poster.reducer';
import * as fromThingsReducer from './things.reducer';

// pages
import * as fromPosterPageReducer from './poster.page.reducer';

/**
 * As mentioned, we treat each reducer like a table in a database. This means
 * our top level state interface is just a map of keys to inner state types.
 */
export interface ApplicationState1 {
  parts: fromPartsReducer.State;
  posters: fromPosterReducer.State;
  things: fromThingsReducer.State;
  posterPage: fromPosterPageReducer.State;
}

/**
 * Because metareducers take a reducer function and return a new reducer,
 * we can use our compose helper to chain them together. Here we are
 * using combineReducers to make our top level reducer, and then
 * wrapping that in storeLogger. Remember that compose applies
 * the result from right to left.
 */
export const reducers: ActionReducerMap<ApplicationState1> = {
  parts: fromPartsReducer.PartsReducer,
  posters: fromPosterReducer.PostersReducer,
  things: fromThingsReducer.ThingsReducer,
  posterPage: fromPosterPageReducer.PosterPageReducer
};

// Parts

export const selectParts = (state: ApplicationState1) => state.parts;
export function getPartsState(state$: Observable<ApplicationState1>) {
  return state$.map(state => state.parts);
}
export function getPosterState(state$: Observable<ApplicationState1>) {
  return state$.map(state => state.posters);
}

export const getParts = (state: ApplicationState1) => state['constructor']['parts'];
export const getPartsSelector = (state: ApplicationState1) => state['constructor']['parts']['parts'];
// Poster
export const getPostersSelector = (state: ApplicationState1) => state['constructor']['posters']['posters'];
export const getPostersAreLoadedSelector = (state: ApplicationState1) => state['constructor']['posters']['postersAreLoaded'];

// Things
export const getThingsSelector = (state: ApplicationState1) => state['constructor']['things']['things'];

// Pages
export const getPosterPagePosterSelector = (state: ApplicationState1) => state['constructor']['posterPage']['poster'];

// ISO
// export const getISOsSelector = (state: ApplicationState1) => state['constructor']['iso']['iso'];

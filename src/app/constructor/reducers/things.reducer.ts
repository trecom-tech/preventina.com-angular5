import { Action } from '@ngrx/store';
// import '@ngrx/core/add/operator/select';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import * as _ from 'lodash';

import * as thingsActions from '../actions/things.actions';
import { ThingType } from '../models/thing.type';

export interface State1 {
  things: ThingType[];
  thing: ThingType;
}

export interface State {
  things: ThingType[];
}
const initialState: State = {
  things: []
};

export interface ActionWithPayload<T> extends Action {
  payload: T;
};

/**
 * THINGS_LOAD_SUCCESS: Set things as payload
 * THING_UPDATE: Update a thing in things
 */

export function ThingsReducer(state = initialState, action: ActionWithPayload<State1>): State {
  switch (action.type) {
    case thingsActions.ThingsActionTypes.THINGS_LOAD_SUCCESS: {
      const datas = action.payload.things;
      for (let i = 0; i < datas.length; i++) {
        if (datas[i].part === 'gender') {
          datas[i].template = true;
          datas[i].url = '/assets/gear-templates/' + datas[i].gender + '/body/' + 'Body_skin_01.svg';
        } else if (datas[i].part === 'skin') {
          datas[i].template = true;
          datas[i].url = '/assets/gear-templates/' + datas[i].gender + '/body/' + 'Body_skin_0' + datas[i].counter + '.svg';
        } else if (datas[i].part === 'bodypart') {
          datas[i].template = true;
          datas[i].url = '/assets/gear-templates/' + datas[i].gender + '/body/' + 'Hand_skin_0' + datas[i].counter + '.svg';
        } else {
          datas[i].template = true;
          datas[i].url = '/assets/gear-templates/' + datas[i].gender + '/' + datas[i].name + '.svg';
        }
      }
      return {things: datas};
    }

    case thingsActions.ThingsActionTypes.THINGS_LOAD_FAIL: {
      return state;
    }

    case thingsActions.ThingsActionTypes.THING_UPDATE: {
      let thingIndex = -1;
      const thing = action.payload.thing;
      const things = state.things;
      for (let i = 0; i < things.length; i++) {
        if (things[i].uuid === thing.uuid) {
          thingIndex = i;
          break;
        }
      }
      if (thingIndex !== -1) {
        return {
          things: [
            ...things.slice(0, thingIndex),
            thing,
            ...things.slice(thingIndex + 1)
          ]
        };
      } else {
        return state;
      }
    }

    default: {
      return state;
    }
  }
}

export function getThings(state$: Observable<State>) {
  return state$.map(s => s.things);
}


import * as _ from 'lodash';

function getStartGender() {
  return _.assign({}, {
    'uuid': 'woman_gender',
    'part': 'gender',
    'gender': 'woman'
  });
};

function getSstartSkin() {
  return _.assign({}, {
    'uuid': 'woman_skin_03',
    'part': 'skin',
    'gender': 'woman',
    'counter': 3,
    'associateId': 'man_skin_03',
    'template': true,
    'url': '/assets/gear-templates/woman/body/Body_skin_03.svg',
    'layer': 0
  });
}

function getStartStuff() {
  return [_.assign({}, {
      'uuid': 'woman_hand_03',
      'part': 'bodypart',
      'gender': 'woman',
      'counter': 3,
      'associateId': 'man_hand_03',
      'template': true,
      'url': '/assets/gear-templates/woman/body/Hand_skin_03.svg',
      'layer': 100
    })];
}

export function getDefaultPoster() {
  return {
    orientation: 'PORTRAIT',
    gender: getStartGender(),
    skin: getSstartSkin(),
    parts: [],
    stuff: getStartStuff(),
    iso: []
  };
}


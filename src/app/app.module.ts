import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Type } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { StoreModule } from '@ngrx/store';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { ModalModule } from 'ngx-bootstrap/modal';
import { EffectsModule } from '@ngrx/effects';
import { CovalentHttpModule, IHttpInterceptor } from '@covalent/http';

import { reducers } from './common/reducers';
import { AuthEffect } from './common/effects/';

import { AppComponent } from './app.component';
import { LoginContainerComponent } from './login';


import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';

import { SharedModule } from './common/share.module';

import {
  AuthService,
  UserService,
  NotificationService,
  ThingsColouringService,
  GearComponentService,
  AuthInterceptor,
  ContactService,
  FireBasePosterService,

  FirebaseService,
  BackentinaService,

  // guards
  LoginGuard,
  AuthorisedGuard,

  // endpoints
  UserEndpoint,
  PosterEndpoint,
  // pipes
  FirstLetter,
  // OrderBy
} from './common';
import {AppRoutingModule} from './app-routing.module';
import {WebsiteModule} from './website/website.module';

const APP_COMPONENTS = [
  AppComponent,
  LoginContainerComponent,
  // pipes
  FirstLetter
];

const httpInterceptorProviders: Type<IHttpInterceptor>[] = [
  AuthInterceptor
];

const APP_PROVIDERS = [
  AuthService,
  UserService,
  NotificationService,
  FirebaseService,
  BackentinaService,
  UserEndpoint,
  PosterEndpoint,
  ThingsColouringService,
  GearComponentService,
  ContactService,
  FireBasePosterService,
  httpInterceptorProviders,

  // guards
  LoginGuard,
  AuthorisedGuard
];

export const firebaseCredentials = {
  apiKey: 'AIzaSyB0Gh1qJB8CO65U7fcBXwgHE8RxVJ8roUk',
  authDomain: 'preventina-9573b.firebaseapp.com',
  databaseURL: 'https://preventina-9573b.firebaseio.com',
  projectId: 'preventina-9573b',
  storageBucket: 'preventina-9573b.appspot.com',
  messagingSenderId: '590741124640'
}

@NgModule({
  declarations: APP_COMPONENTS,
  imports: [
    BrowserModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    HttpClientModule,
    StoreModule.forRoot(reducers),
    EffectsModule.forRoot([
      AuthEffect,
    ]),
    AccordionModule.forRoot(),
    ModalModule.forRoot(),
    CovalentHttpModule.forRoot({
      interceptors: [{
        interceptor: AuthInterceptor, paths: ['**'],
      }],
    }),
    AngularFireModule.initializeApp(firebaseCredentials),
    AngularFireDatabaseModule,
    WebsiteModule,
    AppRoutingModule
  ],
  providers: [
    APP_PROVIDERS
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

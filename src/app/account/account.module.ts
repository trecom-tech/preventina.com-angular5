import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { AccountContainerComponent } from './containers/account';
import { EditableTitleComponent, PostersListComponent } from './components';
import { PosterSvgBuilder } from '../common/components';
import { SharedModule } from '../common/share.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild([
      { path: '', component: AccountContainerComponent },
    ]),
    SharedModule
  ],
  declarations: [
    AccountContainerComponent,
    EditableTitleComponent,
    PostersListComponent,
  ],
  providers: [],
})

export class AccountModule {}

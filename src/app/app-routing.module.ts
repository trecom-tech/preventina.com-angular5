import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import {HowComponent} from './website/how/how.component';
import {AuthorisedGuard} from './common/guards/account.guard';

const appRoutes: Routes = [

  {
    path: 'constructor',
    loadChildren: './constructor/constructor.module#ConstructorModule'
  },
  /*{
    path: 'how',
    component: HowComponent
  },
  {
    path: 'pricing',
    component: PricingComponent
  },*/
  {
    path: 'account',
    loadChildren: './account/account.module#AccountModule',
    canActivate: [AuthorisedGuard]
  },
  {
    path: '**',
    component: HowComponent
  },
  { path: '',   redirectTo: '/home', pathMatch: 'full' }
];

@NgModule({
  imports: [
    RouterModule.forChild(
      appRoutes
    )
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {}

import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Effect, Actions } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mapTo';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/take';
import { Observable } from 'rxjs/Observable';
import * as _ from 'lodash';

import { AuthService } from '../services';
import { UserEndpoint } from '../endpoints';
import * as authActions from '../actions/auth.actions';

@Injectable()
export class AuthEffect {
  constructor(
    private actions$: Actions,
    private authService: AuthService,
    private userEndpoint: UserEndpoint,
    private router: Router
  ) {}

  @Effect() init$: Observable<Action> = this.actions$
    .take(1)
    .mapTo(this.authService.getAuthToken())
    .map(token => !_.isEmpty(token) ? new authActions.AuthCheckHasTokenAction(token) : new authActions.AuthCheckHasNoTokenAction());

  @Effect() 
    appHasToken$ = this.actions$
    .ofType(authActions.AuthActionTypes.AUTH_CHECK_HAS_TOKEN)
    .map((action: authActions.AuthCheckHasTokenAction) => action.payload)
    .do((payload) => new authActions.AuthLoadUserAction(payload));

  @Effect() 
    loadUserIfThereIsToken$ = this.actions$
    .ofType(authActions.AuthActionTypes.AUTH_LOAD_USER)
    .map((action: authActions.AuthLoadUserAction) => action.payload)
    .switchMap((payload)  => {
      return this.userEndpoint.getCurrentUser(payload)
          .map(user => new authActions.AuthLoadUserSuccessAction(user))
          .catch(error => Observable.of(new authActions.AuthLoadUserFailAction()));
    });

  @Effect() userLoadFail$: Observable<Action> = this.actions$
    .ofType(authActions.AuthActionTypes.AUTH_LOAD_USER_FAIL)
    .map(() => new authActions.AuthCleanAction());

  @Effect() 
    setToken$ = this.actions$
    .ofType(authActions.AuthActionTypes.AUTH_SET_TOKEN)
    .map((action: authActions.AuthSetTokenAction) => action.payload)
    .do((payload) => this.authService.putTokenToStorage(payload, 365));

  @Effect({ dispatch: false }) authClean$: Observable<Action> = this.actions$
    .ofType(authActions.AuthActionTypes.AUTH_CLEAN)
    .do(() => {
      this.authService.removeAuthToken();
      console.log('EVENT_USER_IS_NOT_AUTHORISED');
    });

  @Effect() 
    login$ = this.actions$
    .ofType(authActions.AuthActionTypes.AUTH_LOGIN)
    .map((action: authActions.AuthLoginAction) => action.payload)
    .switchMap((payload) => {
          return this.userEndpoint.login(payload)
            .map(user => new authActions.AuthLoginSuccessAction(user))
            .catch(error => Observable.of(new authActions.AuthLoginFailAction()));
    });
    // .switchMap(({ payload }) => {
    //     return this.userEndpoint.login(payload)
    //       .map(user => new authActions.AuthLoginSuccessAction(user))
    //       .catch(error => Observable.of(new authActions.AuthLoginFailAction()));
    //   }
    // );

  @Effect({ dispatch: false }) 
    loginSuccess$ = this.actions$
    .ofType(authActions.AuthActionTypes.AUTH_LOGIN_SUCCESS)
    .map((action: authActions.AuthLoginSuccessAction) => action.payload)
    .do((payload) => {
      this.authService.putTokenToStorage(payload.dailyKey, 365);
      this.router.navigate(['']);
    });
}

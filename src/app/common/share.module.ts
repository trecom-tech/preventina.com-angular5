import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { PosterSvgBuilder, DynamicGearComponent, ExtendedInputComponent, UserNavComponent } from './components';
import { OrderBy } from './pipes';
import {HeaderComponent} from "./components/header/header.component";

@NgModule({
  imports: [
    CommonModule,
    RouterModule
  ],
  declarations: [
    PosterSvgBuilder,
    DynamicGearComponent,
    ExtendedInputComponent,
    OrderBy,
    UserNavComponent,
    HeaderComponent
  ],
  exports: [
    PosterSvgBuilder,
    DynamicGearComponent,
    ExtendedInputComponent,
    OrderBy,
    UserNavComponent,
    HeaderComponent
  ],
})

export class SharedModule {}

import { Component, Input, ViewEncapsulation, OnInit, AfterViewInit, OnChanges } from '@angular/core';

import { PosterType } from '../../../constructor/models';
import { FirebaseService } from '../../../common/services';

import { has } from 'lodash';

@Component({
  selector: 'poster-builder',
  templateUrl: './poster-builder.html',
  styleUrls: [ './poster-builder.scss' ],
  encapsulation: ViewEncapsulation.None
})
export class PosterSvgBuilder implements OnInit, OnChanges {
  /**
   * @Input
   */
  @Input() poster: PosterType;

  constructor(public firebaseService: FirebaseService) {}

  /**
   * @return whether show this tab in poster builder
   * If it is not gender, return true
   * otherwise return whether poster.skin is selected
   */
  showTab(part): boolean {
    if (part.type !== 'gender') {
      return true;
    }
    return !has(this.poster.skin, 'uuid'); // test if uuid exists to check if a skin is selected
  }

  ngOnInit() {
    console.log(this.poster);
  }

  ngOnChanges(changes) {
    if (changes.colours) {
      console.log(changes);
    } else if (changes.gear) {
      console.log(changes);
    } else {
      console.log(changes);
    }
  }
}

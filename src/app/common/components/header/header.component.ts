import { Component, OnInit, Input } from '@angular/core';
import { FirebaseService } from '../../services/firebase.service';
import * as PosterActions from '../../../constructor/actions/poster.actions';
import { Store } from '@ngrx/store';
import { ApplicationState1 } from '../../../constructor/reducers';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(
    private store: Store<ApplicationState1>
  ) { }

  ngOnInit() {
  }

}

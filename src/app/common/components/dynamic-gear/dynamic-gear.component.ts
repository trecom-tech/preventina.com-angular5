import { Component, Input, Output, EventEmitter, OnInit, AfterViewInit, OnChanges, ElementRef } from '@angular/core';

import { ThingType, DimensionType, GenderType, SvgType } from '../../../constructor/models';
import { ThingsColouringService } from '../../services/things-colouring.service';
import * as _ from 'lodash';

@Component({
  selector: 'dynamic-gear',
  templateUrl: './dynamic-gear.component.html',
  styleUrls: [ './dynamic-gear.scss' ],
})

export class DynamicGearComponent implements OnInit, AfterViewInit, OnChanges {
  /**
   * @Input
   */
  @Input() gear: ThingType;
  /**
   * @Input
   */
  @Input() coloursObservable: any;
  /**
   * @Output
   */
  @Output() select: EventEmitter<ThingType> = new EventEmitter(false);

  /**
   * jQuery object of the component element
   */
  // private jqueryElRef: any;

  constructor(
    private elementRef: ElementRef,
    private thingsColouringService: ThingsColouringService
  ) {
  }

  /**
   * Sets jqueryElRef, subscribes coloursObservable, calls changeColours and setDimensions
   */
  ngAfterViewInit() {

    /*if (this.coloursObservable) {
      this.coloursObservable.subscribe((colours: string[]) => {
        this.changeColours(this.elementRef.nativeElement, colours);
      });
    }*/

    /*setTimeout(() => {
      this.fetchGear(this.elementRef.nativeElement, [this.gear.firstColour, this.gear.secondColour, this.gear.thirdColour]);
      // this.changeColours(this.elementRef.nativeElement, [this.gear.firstColour, this.gear.secondColour, this.gear.thirdColour]);
    });*/
  }

  /**
   * Calls changeColours and setDimensions if necessary
   */
  ngOnChanges(changes) {
    if ((changes.colours && changes.colours.currentValue)) {
      setTimeout(() => {
        this.fetchGear(this.elementRef.nativeElement, changes.colours && changes.colours.currentValue || [this.gear.firstColour, this.gear.secondColour, this.gear.thirdColour]);
      });
      /*setTimeout(() => {
        this.changeColours(this.elementRef, changes.colours && changes.colours.currentValue || [this.gear.firstColour, this.gear.secondColour, this.gear.thirdColour]);
      });*/
    } else if (changes.gear) {
      // this.gear = changes.gear.currentValue ? changes.gear.currentValue : changes.gear.previousValue;
      setTimeout(() => {
        this.fetchGear(this.elementRef.nativeElement, [changes.gear.currentValue.firstColour, changes.gear.currentValue.secondColour, changes.gear.currentValue.thirdColour]);
      });
    }
  }

  ngOnInit() {
  }

  /**
   * Calls thingsColouringService.changeColourOfGradients
   */

  private fetchGear(native: any, [firstColour, secondColour, thirdColour]: string[]) {
    console.log('elRef = ' + native.nativeElement);
    console.log('svg container = ' + $(native).find('.poster-content div'));
    const cs = this.thingsColouringService;
    const gearUrl = $(native).find('.poster-content div').prop('title');

    const changeColours = function(aRef: ElementRef, [aFirstColour, aSecondColour, aThirdColour]: string[], aCs: ThingsColouringService) {
      if (aFirstColour) {
        aCs.changeColourOfGradients($(native), '.color1 stop', aFirstColour);
      }

      if (aSecondColour) {
        aCs.changeColourOfGradients($(native), '.color2 stop', aSecondColour);
      }

      if (aThirdColour) {
        aCs.changeColourOfGradients($(native), '.color3 stop', aThirdColour);
      }
    };

    if (gearUrl) {
      fetch(gearUrl.toString()).then(function (response) {
        return response.text();
      }).then(function (svg: string) {
        $(native).find('.poster-content div').html(svg);
        changeColours(native, [firstColour, secondColour, thirdColour], cs);
      })
      .catch(function (err) {
        // Error :(
      });
    }

  }
}

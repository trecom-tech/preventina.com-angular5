export interface ContactType {
  name?: string;
  email?: string;
  question?: string;
  date?: string;
}

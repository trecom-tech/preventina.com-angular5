import { Injectable } from '@angular/core';
import {PosterType} from "../../constructor/models/poster.type";
import {AngularFireDatabase, AngularFireObject} from "angularfire2/database";
import {ThingType} from "../../constructor/models/thing.type";

@Injectable()
export class FireBasePosterService {

  posterRef: AngularFireObject<PosterType>;

  constructor(private db: AngularFireDatabase) {
    this.posterRef = db.object('poster');
  }

  doCreatePoster(poster: PosterType) {
    poster.creationDate = new Date().toDateString();
    poster.skin.firstColour = null;
    poster.skin.secondColour = null;
    poster.skin.thirdColour = null;
    poster.gender.firstColour = null;
    poster.gender.secondColour = null;
    poster.gender.thirdColour = null;
    poster.stuff.forEach((item: ThingType) => {
      if (item.secondColour === undefined) {item.secondColour = null; }
      if (item.thirdColour === undefined) {item.thirdColour = null; }
    });
    const contactKey = this.db.list('/posters').push(poster).key;
  }

}

import { Injectable } from '@angular/core';
import {AngularFireDatabase, AngularFireObject} from 'angularfire2/database';
import {ContactType} from "../models";

@Injectable()
export class ContactService {

  contactRef: AngularFireObject<ContactType>;

  constructor(private db: AngularFireDatabase) {
    this.contactRef = db.object('contact');
  }

  doContact(contact: ContactType) {
    contact.date = new Date().toDateString();
    const contactKey = this.db.list('/contacts').push(contact).key;
  }

}

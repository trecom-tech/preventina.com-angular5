import { Injectable } from '@angular/core';
import 'rxjs/add/operator/let';
import { Store } from '@ngrx/store';
import { PosterType } from '../../constructor/models';
import { PartType, ThingType, SvgType, CommentType } from '../../constructor/models';
import { ApplicationState } from '../reducers';
import {ApplicationState1, getParts} from '../../constructor/reducers';
import { getPartsSelector, getThingsSelector, getPostersSelector, getPostersAreLoadedSelector } from '../../constructor/reducers';
import * as partsActions from '../../constructor/actions/parts.actions';
import * as posterActions from '../../constructor/actions/poster.actions';
import * as svgActions from '../../constructor/actions/svgs.actions';
import { groupBy, each } from 'lodash';
import { Observable } from 'rxjs/Observable'
import * as _ from 'lodash';

import { AngularFireDatabase,
         AngularFireList } from 'angularfire2/database';

@Injectable()
export class FirebaseService {
  public parts$: Observable<PartType[]> = this.store1.select(getPartsSelector);

  constructor(
    private store: Store<ApplicationState>,
    private store1: Store<ApplicationState1>,
    private db: AngularFireDatabase
  ) {}

  /* SVGS */

  getSvgs(): Observable<SvgType[]> {
    return this.db.list('/svgs').valueChanges();
  }

  loadSvg(params: any) {
    const gear = params.gear;
    const gender = params.gender;
    return this.db.object('/svgs/${gear}/${gender}').valueChanges();
    // return this.store.dispatch(new svgActions.SvgsLoadAction({"gender": gender, "gear": gear}));
  }


  /* PARTS */

  getParts(): Observable<any[]>{
    return this.db.list('/parts').valueChanges();
  }

  reorder(parts: PartType[]) {
    this.store.dispatch(new partsActions.PartsReorderAction(parts));
  }

  selectPart(key: string) {
    this.getParts().subscribe((data) => data.find((aPart) => {
      return aPart.uuid === key;
    }));
  }

  /* THINGS */

  public things$: Observable<ThingType[]> = this.store1.select(getThingsSelector);

  groupedThings$: Observable<any> = this.things$.map(things => {
      const groupedGears = groupBy(things, 'part');

      each(groupedGears, (value, key) => {
        if (key !== 'gender') {
          groupedGears[key] = <any>groupBy(value, 'gender');
        }
      });

    return groupedGears;
  });

  getThings(): Observable<any[]> {
  	return this.db.list('/gears').valueChanges();
  }

  /* POSTERS */

  posters$: Observable<PosterType[]> = this.store1.select(getPostersSelector);
  postersAreLoaded$: Observable<boolean> = this.store1.select(getPostersAreLoadedSelector);


  /**
   * Load all posters
   */
  loadPosters() {
    //return this._http.get(API_FIREBASE_URL + "loadposters").map(response => response.json());
  	return this.db.list('/loadposters').valueChanges();
  }

  createPoster(data) {
  	const itemRef = this.db.object('posters');
		itemRef.set(data)
    .then((obj) => {})
		.catch(err => console.log(err, 'Something went WRONG!'));
    return itemRef.valueChanges();
    //return this._http.post(API_FIREBASE_URL + "createposter", data).map(response => response.json());
  }

  /**
   * Load a single poster by its ID
   */
  loadPoster(posterId: number) {
    this.store.dispatch(new posterActions.PosterLoadAction(posterId));
  }

  managePart(posterId, part: PartType) {
    this.store.dispatch(new posterActions.PosterManagePartAction({ posterId, part}));
  }

  addComment(comment: CommentType, posterId?: any) {
    this.store.dispatch(new posterActions.PosterAddCommentAction({comment, posterId}));
  }

  modifyComment(index: number, text: string, posterId?: any) {
    this.store.dispatch(new posterActions.PosterModifyCommentActions({index, text, posterId}));
  }

  deleteComment(index: number, posterId?: any) {
    this.store.dispatch(new posterActions.PosterDeleteCommentActions({index, posterId}));
  }

  selectThing(posterId, thing: ThingType) {
    this.store.dispatch(new posterActions.PosterChangeThingsAction({ posterId, thing }));
  }

  deselectThing(posterId, thing: ThingType) {
    this.store.dispatch(new posterActions.PosterChangeThingsAction(_.assign({}, { posterId, thing})));
  }

  changeThingCommentPosition(payload: { thingType: string; position: [number, number]; }) {
    this.store.dispatch(new posterActions.PosterChangeThingCommentPositionAction(payload));
  }


  changeThingPosition(payload: { thingType: string; position: [number, number]; }) {
    this.store.dispatch(new posterActions.PosterChangeThingCommentPositionAction(payload));
  }

  updatePosterWithoutSaveInDB(poster: PosterType) {
    this.store.dispatch(new posterActions.PosterUpdateStoreAction(poster));
  }

  savePoster(poster: PosterType) {
    if (poster.id) {
      this.updatePoster(poster);
    } else {
      this.createPoster(poster);
    }
  }

  // createPoster(poster: PosterType) {
  //   this.store.dispatch(new posterActions.PosterCreateAction(poster));
  // }

  updatePoster(poster: PosterType) {
    this.store.dispatch(new posterActions.PosterUpdateAction(poster));
  }

  deletePoster(poster: PosterType) {
    this.store.dispatch(new posterActions.PosterDeleteAction(poster));
  }



}

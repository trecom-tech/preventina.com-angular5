import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

import { ApplicationState1 } from '../../constructor/reducers';
import { getPosterPagePosterSelector } from '../../constructor/reducers';
import * as posterPageActions from '../../constructor/actions/poster.page.actions';
import { PosterType } from '../../constructor/models';
import { StripeTokenType } from '../../constructor/models/stripe-token.type';

@Injectable()
export class PosterPageService {
  poster$: Observable<PosterType> = this.store.select(getPosterPagePosterSelector);
  closeNewPosterNameModalSubject: Subject<any> = new Subject();
  openNewPosterNameModalSubject: Subject<any> = new Subject();


  constructor(
    private store: Store<ApplicationState1>,
  ) {
  }

  getPoster(id: number) {
    this.store.dispatch(new posterPageActions.PosterPageGetAction(id));
  }

  cleanPosterPage() {
    this.store.dispatch(new posterPageActions.PosterPageCleanAction());
  }

  cleanPosterPageStripeData() {
    this.store.dispatch(new posterPageActions.PosterPageCleanStripeAction());
  }

  makePoster() {
    this.store.dispatch(new posterPageActions.PosterPageMakePosterAction());
  }
}

import { Headers } from '@angular/http';
import * as _ from 'lodash';
import { ThingType, PartType, CommentType, ISOType } from '../../constructor/models';
import { PosterType } from '../../constructor/models';

import { getDefaultPoster } from '../../constructor/default-data';


export const makeDefaultHeaders = () => {
  return { headers: new Headers({ 'Content-Type': 'application/json' }) };
};


const typeCache: { [label: string]: boolean } = {};

/**
 * This function coerces a string into a string literal type.
 * Using tagged union types in TypeScript 2.0, this enables
 * powerful typechecking of our reducers.
 *
 * Since every action label passes through this function it
 * is a good place to ensure all of our action labels
 * are unique.
 */
export function type<T>(label: T | ''): T {
  if (typeCache[<string>label]) {
    throw new Error(`Action type "${label}" is not unqiue"`);
  }

  typeCache[<string>label] = true;

  return <T>label;
}

export function makeDefaultPoster() {
  return _.assign({
    tempId: (91253123 + _.random(100500) * 39812).toString(),
  }, getDefaultPoster());
}

export function hasGearInPoster(poster: PosterType, gear: ThingType) {
  return _.some(poster.stuff, {'uuid': gear.uuid});
}

/*export function hasGearInPart(posterPart: PosterPartType, gear: ThingType){
 return _.find(posterPart.things, (t: ThingType) => {
 return _.isEqual(t, gear);
 });
 }*/

export function getGearFromPoster(poster: PosterType, uuid: string) {
  return _.find(poster.stuff, (t: ThingType) => {
    return t.uuid === uuid;
  });
}

export function isEqual(oldThing: ThingType, newThing: ThingType) {
  return oldThing.firstColour === newThing.firstColour &&
    oldThing.secondColour === newThing.secondColour &&
    oldThing.thirdColour === newThing.thirdColour;
}

export function addPartToPoster(poster: PosterType, part: PartType) {
  _.remove(poster.parts, (p) => {
    return p.uuid === part.uuid;
  });
  poster.parts.push(part);
}

export function addGearToPoster(poster: PosterType, thing: ThingType) {
  if (!poster['stuff']) {
    poster['stuff'] = [];
  }
  poster.stuff.push(thing);
}

export function removeGearFromPoster(poster: PosterType, thing: ThingType) {
  if (poster && poster.stuff) {
    _.remove(poster.stuff, (t) => {
      return t.uuid === thing.uuid;
    });
  }
}

export function getAllUsedThingsAlternatives(poster: PosterType) {
  const allUsedThings = [];
  _.each(poster.stuff, (thing: ThingType) => {
    allUsedThings.push(thing.associateId);
  });
  return allUsedThings;
}

export function getAllUsedThings(poster: PosterType) {
  const allUsedThings: ThingType[] = [];
  _.every(poster.stuff, (thing: ThingType) => {
    allUsedThings.push(thing);
  });
  return allUsedThings;
}

export function addComment(poster: PosterType, comment: CommentType ) {
  if (poster.comment) {
    poster.comment.push(comment);
  } else {
    const cmt: CommentType[] = [];
    cmt.push(comment);
    poster['comment'] = cmt;
  }
}

export function modifyComment(poster: PosterType, index: number, text: string) {
  poster.comment[index].content = text;
}

export function removeComment(poster: PosterType, index: number) {
  if (poster) {
    _.remove(poster.comment, (t, i) => {
      return i === index;
    });
  }
}

export function addISOToPoster(poster: PosterType, iso: ISOType) {
    _.remove(poster.iso, (p) => {
        return p.uuid === iso.uuid;
    });
    poster.parts.push(iso);
}
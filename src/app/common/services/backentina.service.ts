import {Injectable} from "@angular/core";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import {StripeTokenType} from "../../constructor/models/stripe-token.type";
import {PosterType} from "../../constructor/models/poster.type";

@Injectable()
export class BackentinaService {

  private apiendpoint = 'https://preventina.com/app/createPdf';
  // private apiendpoint = 'http://localhost:8080/app/createPdf';

  constructor(private http: HttpClient) {
  }


  makePoster(data: PosterType): Observable<any> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json;charset=UTF-8' });
    return this.http.post(this.apiendpoint, data, { headers });
  }


}

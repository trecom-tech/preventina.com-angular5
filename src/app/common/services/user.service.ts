import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';

import { UserType } from '../models';
import { ApplicationState } from '../reducers';
import { getUserSelector } from '../reducers';
import * as authActions from '../actions/auth.actions';

@Injectable()
export class UserService {
  user$: Observable<UserType> = this.store.select(getUserSelector);
  isAuth$: Observable<boolean> = this.user$.map((user: UserType) => !!user);

  constructor(private store: Store<ApplicationState>) {}

  login(data) {
    this.store.dispatch(new authActions.AuthLoginAction(data));
  }

  logout() {
    this.store.dispatch(new authActions.AuthCleanAction());
  }
}

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pricing',
  templateUrl: './pricing.component.html',
  styleUrls: ['./pricing.component.scss']
})
export class PricingComponent implements OnInit {

  sponsEur = '5.90';
  tempEur = '19.90';
  fullEur = '39.90';
  sponsUsd = '6.90';
  tempUsd = '24.90';
  fullUsd = '49.90';

  eu = [this.sponsEur, this.tempEur, this.fullEur];
  us = [this.sponsUsd, this.tempUsd, this.fullUsd];

  current = this.eu;
  currentCurrency = 'eu';

  constructor() { }

  toggleCurrency(): void {
    console.log("CLICK!");
    if (this.currentCurrency === 'eu') {
      this.current = this.us;
      this.currentCurrency = 'us';
    } else {
      this.current = this.eu;
      this.currentCurrency = 'eu';
    }
  }

  ngOnInit() {
  }

}

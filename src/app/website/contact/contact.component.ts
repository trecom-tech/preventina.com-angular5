import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ContactService} from "../../common/services/contact.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {

  contactForm: FormGroup;
  ini = true;

  constructor(private contactService: ContactService, private fb: FormBuilder, private router: Router) { }

  ngOnInit() {
    this.buildForm();
  }

  doContact() {
    if (this.contactForm.status !== 'VALID') {
      this.ini = false;
      return;
    }

    const data = this.contactForm.value;
    this.contactService.doContact(data);
    this.buildForm();

    this.router.navigate(['/success']);

  }


  private buildForm() {
    this.contactForm = this.fb.group({
      name:    ['', Validators.required ],
      email:  ['', Validators.email],
      question:    ['', Validators.required ]
    });
  }

}

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HowComponent } from './how/how.component';
import { PricingComponent } from './pricing/pricing.component';
import {DemoComponent} from "./demo/demo.component";
import {ContactComponent} from "./contact/contact.component";
import {HomeComponent} from "./home/home.component";
import {SuccessComponent} from "./success/success.component";


const websiteRoutes: Routes = [

  {
    path: 'pricing',
    component: PricingComponent
  },
  {
    path: 'success',
    component: SuccessComponent,
  },
  {
    path: 'how',
    component: HowComponent,
  },
  {
    path: 'demo',
    component: DemoComponent,
  },
  {
    path: 'contact',
    component: ContactComponent,
  },
  {
    path: 'home',
    component: HomeComponent,
  },
  {
    path: '',
    component: HomeComponent,
  }
];


@NgModule({
  imports: [
    RouterModule.forRoot(websiteRoutes)
  ],
  exports: [
    RouterModule
  ]
})

export class WebsiteRoutingModule {}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { ModalModule } from 'ngx-bootstrap/modal';
import { SharedModule } from '../common/share.module';
import { PosterPageService } from '../common/services/poster.page.service';
import { GearComponentService } from '../common/services/gear.component.service';

import { HowComponent } from './how/how.component';
import { PricingComponent } from './pricing/pricing.component';
import {WebsiteRoutingModule} from './website-routing.module';
import { DemoComponent } from './demo/demo.component';
import { ContactComponent } from './contact/contact.component';
import { HomeComponent } from './home/home.component';
import { SuccessComponent } from './success/success.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    AccordionModule,
    ModalModule,
    ReactiveFormsModule,
    SharedModule,
    WebsiteRoutingModule
  ],
  declarations: [
    HowComponent,
    PricingComponent,
    DemoComponent,
    ContactComponent,
    HomeComponent,
    SuccessComponent
  ],
  providers: [
    PosterPageService,
    GearComponentService
  ],
})

export class WebsiteModule {}
